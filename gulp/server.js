'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');
var gulpNgConfig = require('gulp-ng-config');

var browserSync = require('browser-sync');
var browserSyncSpa = require('browser-sync-spa');

var util = require('util');
var exec = require('child_process').exec;

var proxyMiddleware = require('http-proxy-middleware');
var parentDir = path.resolve(process.cwd(), '..');

function browserSyncInit(baseDir, browser) {
  browser = browser === undefined ? 'default' : browser;

  var routes = null;
  if(baseDir === conf.paths.src || (util.isArray(baseDir) && baseDir.indexOf(conf.paths.src) !== -1)) {
    routes = {
      '/bower_components': 'bower_components'
    };
  }

  var server = {
    baseDir: baseDir,
    routes: routes,
    middleware: [
      proxyMiddleware('/api', { target: 'http://localhost:3000' })
    ]
  };

  /*
   * You can add a proxy to your backend by uncommenting the line below.
   * You just have to configure a context which will we redirected and the target url.
   * Example: $http.get('/users') requests will be automatically proxified.
   *
   * For more details and option, https://github.com/chimurai/http-proxy-middleware/blob/v0.9.0/README.md
   */
  // server.middleware = proxyMiddleware('/users', {target: 'http://jsonplaceholder.typicode.com', changeOrigin: true});

  browserSync.instance = browserSync.init({
    port: 9000,
    startPath: '/',
    server: server,
    browser: browser
  });
}

/**
* Config environment variable
* RAILS_ENV: staging|production
*
*/
if (process.env.FRAMGIA_STAGING) {
  var options = {env: 'framgia_staging'}
} else {
  var options = {env: process.env.RAILS_ENV || 'staging'}
}

gulp.task('config', function () {
  gulp.src(path.join(conf.paths.src, '/app/config.json'))
    .pipe(gulpNgConfig('skyrec.config', {
      environment: 'local'
    }))
    .pipe(gulp.dest(path.join(conf.paths.src, '/app/')))
});


gulp.task('config:build', function () {
  gulp.src(path.join(conf.paths.src, '/app/config.json'))
    .pipe(gulpNgConfig('skyrec.config', {
      environment: options.env
    }))
    .pipe(gulp.dest(path.join(conf.paths.src, '/app/')))
});

gulp.task('rails', function() {
  exec('rails s', { cwd: parentDir + '/backend' },function(err,stdout,stderr){
    console.log(stdout);
    console.log(stderr);
  });
});

browserSync.use(browserSyncSpa({
  selector: '[ng-app]'// Only needed for angular apps
}));

gulp.task('serve', ['config', 'watch'], function () {
  browserSyncInit([path.join(conf.paths.tmp, '/serve'), conf.paths.src]);
});

gulp.task('serve:dist', ['config:build','build'], function () {
  browserSyncInit(conf.paths.dist);
});

gulp.task('serve:full-stack', ['rails', 'serve']);


gulp.task('serve:e2e', ['config', 'inject'], function () {
  browserSyncInit([conf.paths.tmp + '/serve', conf.paths.src], []);
});

gulp.task('serve:e2e-dist', ['config:build', 'build'], function () {
  browserSyncInit(conf.paths.dist, []);
});
