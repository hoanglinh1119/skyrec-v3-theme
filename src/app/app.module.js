(function () {

  angular.module('skyrec', [
    'ui.router',
    'pascalprecht.translate',
    'skyrec.components',
    'skyrec.pages'
  ])
  .controller('MainController', function ($scope) {
    $scope.options = [
      { icon: 'fa fa-signal', name: 'Signal' },
      { icon: 'fa fa-power-off', name: 'Power Off' },
      { icon: 'fa fa-trash-o', name: 'Trash' },
      { 
        icon: 'fa fa-home', 
        name: 'Home',
        subItems: [
          { icon: 'fa fa-file-o', name: 'File' },
          { icon: 'fa fa-clock-o', name: 'Clock' }
        ]
      }
    ]
  });

})();