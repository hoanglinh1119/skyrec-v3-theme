(function () {
  angular.module('skyrec.components')
    .directive('skrMenuIcon', skrMenuIcon);

  function skrMenuIcon() {
    return {
      restrict: 'E',
      templateUrl: 'app/components/menu-icon/menu-icon.html',
      controller: 'skrMenuIconCtrl',
      scope: {
        options: '='
      },
      link: function (scope, element, attrs) {
        scope.icon = attrs.icon;
      }
    };
  }
  skrMenuIcon.$inject = [];
})();