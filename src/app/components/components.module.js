(function () {

  angular.module('skyrec.components', [
    'ngMaterial',
    'ui.bootstrap'
  ]);

})();