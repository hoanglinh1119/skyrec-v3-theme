(function () {
  angular.module('skyrec.components')
    .directive('skrPageTop', pageTop);

  function pageTop() {
    return {
      retrict: 'E',
      templateUrl: 'app/components/page-top/page-top.html'
    };
  }

})();