(function () {
  angular.module('skyrec.pages', [
    'ngMaterial', 
    'ui.router',

    'skyrec.pages.login'
  ]);
})();