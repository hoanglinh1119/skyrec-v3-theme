(function () {
  angular.module('skyrec.pages.login', []).config(routeConfig);

  function routeConfig($stateProvider) {
    $stateProvider.state('login', {
      url: '/login',
      templateUrl: 'app/pages/login/login.html'
    });
  }
  routeConfig.$inject = ['$stateProvider'];
})();